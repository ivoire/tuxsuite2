import contextlib

from tuxs2.argparse import setup_parser, setup_build_subparser

import sys


def main():
    (parser, cmds) = setup_parser("tuxsuite", "ivoire")
    print(f"{sys.argv=}")

    if "--help" not in sys.argv and not "-h" in sys.argv:
        with contextlib.suppress(IndexError):
            if sys.argv[1] == "build" and sys.argv[2] not in cmds["build"]:
                sys.argv.insert(2, "submit")
            elif sys.argv[1] == "test" and sys.argv[2] not in cmds["test"]:
                sys.argv.insert(2, "submit")
    print(f"{sys.argv=}\n")
    print(parser.parse_known_args())


if __name__ == "__main__":
    sys.exit(main())
