import argparse
from pathlib import Path


__version__ = "0.1"


def key_value(s):
    if s.count("=") != 1:
        error(f"Key Value pair not valid: {s}")
    parts = s.split("=")
    return (parts[0], "=".join(parts[1:]))



def common_options(sp):
    sp.add_argument(
        "-p",
        "--patch-series",
        default=None,
        help=(
            "Patches to apply before building the kernel. Accepts patch "
            "series that applies directly with 'git am' or "
            "'git quiltimport' i.e., a mbox file or directory or gzipped "
            "tarball (.tar.gz)"
        ),
    )
    sp.add_argument(
        "-s",
        "--show-logs",
        default=False,
        action="store_true",
        help="Prints build logs to stderr in case of warnings or errors",
    )
    sp.add_argument(
        "-n",
        "--no-wait",
        default=False,
        action="store_true",
        help="Don't wait for the builds to finish",
    )
    sp.add_argument(
        "-o",
        "--output-dir",
        default=".",
        help="Directory where to download artifacts",
    )
    sp.add_argument(
        "-d",
        "--download",
        default=False,
        action="store_true",
        help="Download artifacts after builds finish",
    )
    sp.add_argument(
        "--json-out",
        help="Write json build status out to a named file path",
        type=argparse.FileType("w", encoding="utf-8"),
    )
    sp.add_argument(
        "--git-head",
        default=False,
        action="store_true",
        help="Build the current git HEAD. Overrrides --git-repo and --git-ref",
    )
    sp.add_argument("--git-sha", help="Git commit")
    sp.add_argument("--git-ref", help="Git reference")
    sp.add_argument("--git-repo", help="Git repository")
    sp.add_argument(
        "-C",
        "--no-cache",
        default=False,
        action="store_true",
        help="Build without using any compilation cache",
    )


def build_cmd_options(sp):
    toolchains = [
        "gcc-8",
        "gcc-9",
        "gcc-10",
        "gcc-11",
        "gcc-12",
        "clang-10",
        "clang-11",
        "clang-12",
        "clang-13",
        "clang-14",
        "clang-nightly",
        "clang-android",
        "rust",
        "rustgcc",
        "rustclang",
        "rustllvm",
    ]

    sp.add_argument(
        "--build-name",
        help="User defined string to identify the build",
        type=str,
    )
    sp.add_argument(
        "--targets",
        nargs="?",
        help="""Targets to build. If no TARGETs are specified, tuxsuite will
        build config + debugkernel + dtbs + kernel + modules + xipkernel.""",
        type=str,
        action="append",
    )
    sp.add_argument(
        "--make-variables",
        nargs="*",
        help="Make variables to use. Format: KEY=VALUE",
        default=[],
        type=key_value,
        action="append",
    )
    sp.add_argument(
        "-e",
        "--environment",
        type=key_value,
        nargs="*",
        help="Set environment variables for the build. Format: KEY=VALUE",
        default=[],
        action="append",
    )
    sp.add_argument(
        "-q",
        "--quiet",
        default=False,
        action="store_true",
        help="Supress all informational output; prints only the download URL for the build",
    )
    sp.add_argument(
        "--toolchain",
        help=f"Toolchain [{'|'.join(toolchains)}]",
    )
    sp.add_argument(
        "--kconfig",
        nargs="?",
        help="Kernel kconfig arguments (may be specified multiple times)",
        type=str,
        default=[],
        action="append",
    )
    sp.add_argument(
        "--kernel-image",
        help="Specify custom kernel image that you wish to build",
    )
    sp.add_argument(
        "--target-arch",
        help="Target architecture [arc|arm|arm64|hexagon|i386|mips|parisc|powerpc|riscv|s390|sh|sparc|x86_64]",
    )
    sp.add_argument(
        "--image-sha",
        default=None,
        help="Pin the container image sha (64 hexadecimal digits)",
    )


def setup_build_subparser(parser):
    # "build config <uid>"
    t = parser.add_parser("config")
    t.add_argument("uid")

    # "build download <uid>"
    t = parser.add_parser("download")
    t.add_argument("--only", default=None, nargs="+")
    t.add_argument("uid")
    t.add_argument("output", type=Path, default=".", nargs="?")

    # "build get <uid>"
    p = parser.add_parser("get")
    p.add_argument("uid")
    p.add_argument("--json", action="store_true")

    # "build list"
    p = parser.add_parser("list")
    p.add_argument("--json", action="store_true")
    p.add_argument("--limit", type=int, default=30)

    # "build logs <uid>"
    t = parser.add_parser("logs")
    t.add_argument("uid")

    # "build metadata <uid>"
    t = parser.add_parser("metadata")
    t.add_argument("uid")

    # "build open <uid>"
    t = parser.add_parser("open")
    t.add_argument("uid")

    # "build submit"
    t = parser.add_parser("submit")
    build_cmd_options(t)
    common_options(t)

    # "build wait <uid>"
    p = parser.add_parser("wait")
    p.add_argument("uid")

    return sorted(parser._name_parser_map.keys())


def setup_test_subparser(parser):
    # "test get <uid>"
    t = parser.add_parser("get")
    t.add_argument("uid")
    t.add_argument("--json", action="store_true")

    # "test list"
    t = parser.add_parser("list")
    t.add_argument("--json", action="store_true")
    t.add_argument("--limit", type=int, default=30)

    # "test logs <uid>"
    t = parser.add_parser("logs")
    t.add_argument("uid")
    t.add_argument("--raw", action="store_true")

    # "test open <uid>"
    t = parser.add_parser("open")
    t.add_argument("uid")

    # "test reproducer <uid>"
    t = parser.add_parser("reproducer")
    t.add_argument("uid")

    # "test results <uid>"
    t = parser.add_parser("results")
    t.add_argument("uid")
    t.add_argument("--raw", action="store_true")

    # "test stderr <uid>"
    t = parser.add_parser("stderr")
    t.add_argument("uid")

    # "test submit"
    t = parser.add_parser("submit")
    t.add_argument("--device", required=True)
    t.add_argument("--kernel", default=None)
    t.add_argument("--modules", default=None)
    t.add_argument("--tests", default=[], nargs="+")
    t.add_argument("--plan", default=None)
    t.add_argument("--build", default=None)
    t.add_argument("--json", action="store_true")
    t.add_argument("--wait", action="store_true")

    # "test wait <uid>
    t = parser.add_parser("wait")
    t.add_argument("uid")

    return sorted(parser._name_parser_map.keys())


def setup_parser(group, project):
    parser = argparse.ArgumentParser(
        prog="tuxsuite",
        description="The TuxSuite CLI is the supported interface to TuxBuild and TuxTest.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s, {__version__}"
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        default=False,
        help="Output critical information only",
    )

    parser.add_argument("--group", default=group)
    parser.add_argument("--project", default=project)

    sub_parser = parser.add_subparsers(dest="command", help="Command")
    sub_parser.required = True

    build = sub_parser.add_parser("build", help="builds").add_subparsers(
        dest="sub_command", help="Commands"
    )
    build.required = True
    build_cmds = setup_build_subparser(build)

    test = sub_parser.add_parser("test", help="tests").add_subparsers(
        dest="sub_command", help="Commands"
    )
    test.required = True
    test_cmds = setup_test_subparser(test)

    return (parser, {"build": build_cmds, "test": test_cmds})
